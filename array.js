// // array is data type which holds multiple values
// // array in js is hetregenous array

// var fruits = ['banana'];
// // array sang sangai aaune kura vaneko index ho
// // inbuilt methods an properties for array
// console.log('check legnth >>', fruits.length);
// console.log('find index >>>', fruits.indexOf('kiwi'));
// console.log('find index of guava >>>', fruits.indexOf('guava'));
// // indexOf is used to calculate first index of items in array
// // console.log('check last index >>', fruits.lastIndexOf('pineapple'));

// // adding elements in array
// // 1 at first 2 at last 3 somewhere in between
// // 1 at first 
// console.log('original array >>', fruits);
// fruits.unshift('mango');
// fruits.unshift('banana');

// // at last
// var finalLength = fruits.push('kiwi');
// console.log('fruits array >>', fruits);
// console.log('final lenghth >>', finalLength);

// // remvoing items from array
// // 1 remove first item
// // 2 remove last item
// // 3 remove from somewhere in between
// console.log('fruits before remove >>', fruits);
// // remvoe first 
// var firstItem = fruits.shift();
// console.log('fruits now ,>>>', fruits);
// console.log('frist item removed >>', firstItem)

// // remove last item
// var lastItem = fruits.pop();
// console.log('fruits >> after pop >', fruits);
// console.log('lasItem >>', lastItem)
// var fruits = ['banana', 'mango', 'kiwi', 'apple', 'pineapple'];

// console.log('fruits before splice >>', fruits);
// // remove or add from somwhere between
// // splice splice is a method tha tis used to insert and remove items from array
// // fruits.splice(fruits.indexOf('kiwi') + 1, 1); // splice for remove
// // fruits.splice(fruits.indexOf('kiwi')+1,0,'licchi','orange','watermelon') splice for add
// fruits.splice(fruits.indexOf('kiwi'),2,'lichhi','watermelon');
// console.log('fruits after splice >>', fruits);

// var hobbies = 'singing,dancing,swimming'
// var hobbeiesArr = hobbies.split(',');
// var backToStr = hobbeiesArr.join(',');
// console.log('to str >>>',backToStr);

var bikes = [
    {
        name: 'fz',
        price: '33',
        color: 'black',
        type: 'sports',
        cc: '250',
        version: 2
    },
    {
        name: 'xr',
        price: '133',
        color: 'red',
        type: 'dirt',
        cc: '190',
        version: 1
    },
    {
        name: 'duke',
        price: '332',
        color: 'orange',
        type: 'sports',
        cc: '250',
        version: 1
    },
    {
        name: 'beneli',
        price: '22',
        color: 'black',
        type: 'sports',
        cc: '250',
        version: 1
    },
    {
        name: 'bullet',
        price: '12',
        color: 'green',
        type: 'classic',
        cc: '350',
        version: 1
    },
    {
        name: 'pulsar',
        price: '122',
        color: 'red',
        type: 'sports',
        cc: '250',
        version: 1
    },
    {
        name: 'crf',
        price: '332',
        color: 'red',
        type: 'dirt',
        cc: '250',
        version: 1
    },
    {
        name: 'cf moto',
        price: '243',
        color: 'black',
        type: 'sports',
        cc: '250',
        version: 1
    }

]
// console.log('bikes .length <>>>', bikes.length);
// for (var i = 0; i < bikes.length; i++) {
//     console.log('i >>', i);
//     console.log(bikes[i])
// }
var a = [];
bikes.forEach(function (item, index) {
    console.log('item is >>>', item);
    console.log('index >>', index);
    item.status = 'availabel';
    if (item.color == 'red' && item.type == 'sports') {
        a.push(item);
    }
})
console.log('bikes NOw >>>', a);

const sportsBike = bikes.filter(function (item, i) {
    if (item.type == 'sports') {
        return true;
    }
});
console.log('sports bike >>>', sportsBike);
const sportsRedbikes = sportsBike.filter(function (item, i) {
    if (item.color == 'red') {
        return item;
    }
});
console.log('red sports bike >>>', sportsRedbikes);
bikes.map(function (item, index) {
    if (item.type == 'sports' && item.color == 'red') {
        item.status = 'sold';
    }
})
console.log('bikes after map', bikes);

// bikes.forEach(function (item, i) {
//     if (item.status == 'sold') {
//         bikes.splice(i, 1);
//     }
// })
console.log('bikes .>>', bikes.length);

var numbers = [1, 2, 34, 5, 6, 76];
var sum = numbers.reduce(function (acc, item) {
    console.log('acc >>>', acc);
    acc.unshift(item);
    return acc;
}, []);
console.log('sum is >>', sum);