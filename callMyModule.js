// import from another file
// syntax es5 require keyword
// syntax es6 import keyword

const importedData = require('./myModule');
console.log('imported data >>>', importedData.fruits);