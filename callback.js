// callback 
// callback is a function which is passed as an argument in another function
// call back is result handling mechnanism for async execution

function buyLaptop(arg) { // function that accepts function as an argument are higher order order funciion in js
    console.log('at laptop shop');
    setTimeout(function() {
        arg();
    }, 3000);

}

buyLaptop(function() {
        console.log('i am result of buyLaptop');
        console.log('i have laptop')
        console.log('start coding');
    }) // a is callback
console.log('i am another task that will not wait for result of buyLaptop');
console.log('eat food');
console.log('wash clothes');


// prepare a story to buy mobile
// shopkeeper told to wait till some hour
// perform some non blocking task
// once you have mobile click photo
// and post in instagram