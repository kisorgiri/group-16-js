// closure is inner function that has access to
// parent function argument
// parent function scope
// global scope
// own argumnet
// own scope

var text = 'welcome';

function welcome(name) {
    var parentScope = 'hi';

    function setLocation(location) {
        var a = 'a';
        console.log(parentScope + ' ' + name + ', ' +
            text + ' ' + 'to ' + location);

        function inner() {
            var c = 'hello i am inner';
            console.log(parentScope + ' ' + name + ', ' +
                text + ' ' + 'to ' + location+' '+c);
        }
        inner();
        // test1 is inner function  
        // inner are named as closure in JS
    }
    setLocation('tinkune');
}

welcome('broadway');