function welcome(name) {
    var innerName = 'brodway infosys nepal';
    // console.log('name is .>', name);
    function place(location) {
        // console.log('location is .>', location);
        // template literal es6 feature
        var data1 = `welcome ${name}, to ${location}`;
        'welcome ' + name + ', to ' + location;
        return data1;
    }

    function getName() {
        return innerName;
    }

    function setName(newName) {
        innerName = newName;
    }
    // var innerRes = place('tinkune');
    // console.log('innerRes', innerRes);
    return {
        place,
        getName,
        setName
    };
}
var res = welcome('test');
console.log('res >>', res.place('nepal'));
console.log('get name before >>>',res.getName());
console.log('set name >>>',res.setName('welcome to brodway'));
console.log('get name after >>>',res.getName());
