

// functional constructor is basic building block of prototype based OOP

// syntax
console.log('global this .>>', this);
function Students() {
    console.log('funcal this >>', this);
    // this keyword here is a constructor which is used to initalized value
    // this.name = 'brodway';

}
// call garda new keyword use hunu paryo

// prtotoype  it is a keyword which is used to add property and methods in functional constructor

Students.prototype.a = 'hi i am a';
Students.prototype.b = 'b';
Students.prototype.getAddress = function (addr) {
    return addr;
}
Students.prototype.test = function () {
    return 'from test';
}

function Teachers(){

}
Teachers.prototype.teching = 'hi i am teaching';
Teachers.prototype = new Students();

var me = new Teachers();
console.log(me.getAddress('hisdklfjasjdf'))

var ram = new Students();
console.log('ram >>>', ram.test());
console.log('ram  teching>>>', ram.teching);
Array.prototype.hi = 'hello';
var fruits = new Array();
console.log('fruits.hi;', fruits.hi);