// // String ==>  
// var str = '  Brodway Infosys Nepal  ';
// console.log('upper case >>', str.toUpperCase());
// console.log('lower case >>', str.toLowerCase())
// console.log('str length', str.length);
// console.log('str trim >>', str.trim().length);

// var hobbies = 'singing&dancing&coding&debugging';
// // type conversion
// // string to Array
// var strToArr = hobbies.split('&');
// console.log('array >>>', strToArr);

// // task 
// var time = '4m23s'; //33m2s // 3m3s 333m3333s
// // prepare a function to calculate minute and sec from above string

// // function calcualte(inputTime){

// // }
// // var res  = calcualte(time);
// // console.log('res >>',res);
// // {
// // 	minute:4,
// // 	sec:23
// // }


// // number
// var num = 23.3333333;
// console.log('int only >>', parseInt(num));
// console.log('float only >>>', parseFloat(num));
// console.log('fixed decimal point >>>', num.toFixed(2));
// var res = num * 3;
// console.log('res >>>', res);
// console.log('is nan >>>',isNaN(res));
// var abc  = '33333';
// console.log('check type >>',typeof(abc));
// console.log(Number(abc))

// //boolean
// var bool = true;
// // !

// object
var student = {
    name: 'ramesh',
    roll: 22,
    address: function() {
        return 'bkt'
    },
    email: 'test@gmail.com'
}
console.log('check property of object', student.hasOwnProperty('namessss'));

// function sendMail(obj) {
//     if ('namess' in obj) {
//     	console.log('exist');
//     }else{
//     	console.log('does not exist');
//     }

// }
// sendMail(student);

// console.log('keys Array >>',Object.keys(student));
// console.log('values array ',Object.values(student));

// Object Serialization
// type conversion object into string
// console.log('orignal >>', student);
// var stringify = JSON.stringify(student);
// console.log('into string ', stringify);
// console.log('back to original >>', JSON.parse(stringify))

// loop(iteration)
// repeated action untill certain condition are not fullfilled

// for in loop is used for every enumerable property
// my recommendation use for in loop for objects only

// for (var key in student) {
//     console.log('key is >>>', key);
//     console.log('value is >>',student[key])
// }

