
var a = 'brodway infosys nepal'; /// orignal value
var b = a; // refrence
// a = 'hi and welcome to broadway infosys nepal';
b = 'hi and welcome to broadway infosys nepal';
console.log('a is >>>', a)
console.log('b is >>>', b)

// immutable properties ===> if original is changed it is not reflected in refrence and vice versa
//  primitive data type

var fruits = ['apple', 'banana', 'mango'];
var fruits1 = fruits;

fruits[0] = 'kiwi';
fruits1[1] = 'kiwi1';
console.log('fruits >>>', fruits);
console.log('fruits1 >>>', fruits1);

// mutable properties ==> if original is cahnged it is reflected to refrence and vice versa
// non-primitive data type array and object are mutable properties in js

var emailData = {
    from: 'kishor',
    to: 'ram',
    subject: 'hi',
    message: 'welcome to brodway'
}

function sendMail(details) {
    details.from = 'brodway';
}
sendMail(emailData);
console.log('email data after call >>>', emailData);