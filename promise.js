// function sendMail(mailDetails, cbErr, cbSuccess) {
//     cbErr('err');
//     cbSuccess('done');
// }

// sendMail({}, function (done) {
//     console.log('failure block');
// }, function (err) {
//     console.log('failure block');
// });


// Promise
// promise is an object which holds result of future of any asynchronous task

// promise has four state /
// pending // promise initialzed vako state
// onRejection // promise // failure failed state
// onFullfilled // pomise success state
// settled // either failed or succedd both means promise is settled

// promise will not change its state after it is either rejected or fullfilled
// promsie will not change its state after it is settled

// promise methods
// .then()  then method is used to handle both success and failure
// recommend to use to handle success
// .catch() catch method is used to handle failure only
// .finally() this method is invoked if promise settled

// syntax
// new keyword used 
function askForMoney() {
    return new Promise(function (success, failure) {
        // success or 1st argument is callback for sucess
        // failure or 2nd argument is callback for failure

        setTimeout(function () {
            // success('succed promsie');
            success("another success");
            failure('failed promise');
        }, 2000);
    })
}

// a
// executon
console.log('a is >>>>', askForMoney());
askForMoney()
    .then(function (data) {
        console.log('success in promise >>', data);
    })
    .catch(function (err) {
        console.log('failure blck', err);
    })
    .finally(function () {
        console.log('promse settled');
    });
