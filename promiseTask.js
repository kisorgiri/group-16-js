function askForNote(topic) {
    var a = new Promise(function (resolve, reject) {
        console.log('friend received a call and insit to callback after he find note');
        setTimeout(function () {
            console.log('note found after hour');
            console.log('now callback to friend who request for it');
            resolve('note');
        }, 2000);
    });
    return a;

}

function printNote(pdfNote, cb) {
    return new Promise(function (resolve, reject) {
        console.log('note is at stationary');
        console.log('shopekeeper told me call back after he finished printing')
        var electricity = true;
        setTimeout(function () {
            if (electricity) {
                resolve('done printing');
            } else {
                reject('no electricity');
            }
        }, 2000);
    });
}

function entertainment(cb) {
    return new Promise(function (resolve, reject) {
        console.log('start entertainment');
        setTimeout(function () {
            reject();
        }, 2000);
    })

}


module.exports = {
    askForNote, printNote, entertainment
}