// scope is accessibility or visibility of application data
// type of scope
// global scope
// local scope
// ===> block scope

// var test = 'i am test'; // global
// var ram = 'i am global ram';
// function welcome(name) { // welcome is global
// 	ram = 'test';
//     console.log('i am test in welcome >>>', ram);
// }

// // welcome();

// function sayHello() {// global
// 	console.log('acces value line in 18 of ram >',ram);
//     console.log('i am in sayHello test >>',test);
// }
// console.log('ram outside >>>',ram);
// sayHello();
// "use strict";

var test = 'gloabl scope'; // global scope

// var undefined = 'hello';
function sayHello(name) {
    // name // local scope
    // let test = 'local scope'; // local scope(functional scope)
    if (name) {
        let test = 'inside if';
        console.log('test in if>>',test)// inside if
    } else {
    	//
        let test = 'inside else';
        console.log('test in  else >>',test)// inside else

    }
    console.log('test is inner >>', test);
    // truthy value => defined
    // falsy value = '', null ,undefined,NaN,false

}
sayHello('');

console.log('test is outer >>', test);

function welcome() {
    var hello = 'i am testing'
}
// welcome();
// console.log('hi is >>', hi);
// console.log('hello is >>', hello);