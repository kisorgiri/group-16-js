function askForNote(topic, cb) {
    console.log('friend received a call and insit to callback after he find note');
    setTimeout(function () {
        console.log('note found after hour');
        console.log('now callback to friend who request for it');
        cb(null, {
            note: topic
        })
    }, 2000);
}

function printNote(pdfNote, cb) {
    console.log('note is at stationary');
    console.log('shopekeeper told me call back after he finished printing')
    var electricity = true;
    setTimeout(function () {
        if (electricity) {
            cb(null, 'printed note');
        } else {
            cb('no electricity');
        }
    }, 2000);

}

function entertainment(cb) {
    console.log('start entertainment');
    setTimeout(function () {
        cb();
    }, 2000);
}


module.exports = {
    askForNote, printNote, entertainment
}